import React, { useState, useEffect } from 'react';
import { Redirect } from 'react-router-dom';

function Projects() {
    const [redirect, setRedirect] = useState(null);
    const [projects, setProjects] = useState([]);
    const [searchTerm, setSearchTerm] = useState('');

    const dateFormatter = date => {
        if (date != null) {
            return new Intl.DateTimeFormat('de-DE', {
                day: "numeric",
                month: "long",
                year: "numeric"
            }).format(new Date(date));
        }
    }
    
    useEffect(() => {
        const fetchProjects = async () => {
            const url = 'https://cloud.sibvisions.com/ProjectsDemo/services/rest/ProjectsDemo/Projects/data/projects/';
            
            const response = await fetch(url, {
                headers: {
                    'Content-Type': 'application/json',
                    'Authorization': JSON.parse(sessionStorage.getItem('auth')).authorization
                }
            });
            
            const data = await response.json();
            
            setProjects(data);
        };

        fetchProjects();
    }, []);
    
    const handleChange = (e) => {
        setSearchTerm(e.target.value.toLowerCase());
    }

    // exclude column list from filter
    const excludeColumns = ['ID','TYP_ID'];

    const filteredData = projects.filter(item => {
        return Object.keys(item).some(key => {
                return (excludeColumns.includes(key) || item[key]==null) ? false : item[key].toLowerCase().includes(searchTerm)
            }
        );
    });
    
    if (redirect) {
        return <Redirect push to={redirect} />
    }

    return (
        <div>
            <h1>Projects</h1>
            <div className="input-group mb-3">
                <div className="input-group-prepend">
                    <span className="input-group-text">Search: </span>
                </div>
                <input
                    type="text"
                    className="form-control"
                    name="search"
                    value={searchTerm}
                    onChange={handleChange}
                />
            </div>
            <table className="table table-hover">
                <thead className="thead-light">
                    <tr>
                        <th>Name</th>
                        <th>Description</th>
                        <th>From Date</th>
                        <th>To Date</th>
                        <th>Typ</th>
                    </tr>
                </thead>
                <tbody>
                    {filteredData.map(project => (
                        <tr key={project.ID} onClick={() => setRedirect(`/projects/${project.ID}`)}>
                            <td>{project.NAME}</td>
                            <td>{project.DESCRIPTION}</td>
                            <td>{dateFormatter(project.FROM_DATE)}</td>
                            <td>{dateFormatter(project.TO_DATE)}</td>
                            <td>{project.TYP_TYP}</td>
                        </tr>
                    ))}
                </tbody>
            </table>
        </div>
    );
}

export default Projects;
