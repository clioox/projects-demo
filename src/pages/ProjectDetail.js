import React, { Fragment, useState, useEffect } from 'react';
import DatePicker from "react-datepicker";
import "react-datepicker/dist/react-datepicker.css";
import { Link } from 'react-router-dom';

function ProjectDetail({ match }) {
    const [project, setProject] = useState({
        NAME: '',
        DESCRIPTION: '',
        FROM_DATE: null,
        TO_DATE: null
    });
    const [typs, setTyps] = useState([]);
    const [todos, setTodos] = useState([]);
        
    useEffect(() => {
        const config = {
            headers: {
                'Content-Type': 'application/json',
                'Authorization': JSON.parse(sessionStorage.getItem('auth')).authorization
            }
        };

        fetch(`https://cloud.sibvisions.com/ProjectsDemo/services/rest/ProjectsDemo/Projects/data/projects/${match.params.id}`, config).then(response => {
            if (!response.ok) {
                throw Error(response.statusText);
            }
            return response.json();
        }).then( data => {
            setProject(data[0]);
        }).catch(error => {
            console.error('Looks like there was a problem: \n', error);
        });
        
        fetch('https://cloud.sibvisions.com/ProjectsDemo/services/rest/ProjectsDemo/Projects/data/projects.subStorages.typ', config).then(response => {
            if (!response.ok) {
                throw Error(response.statusText);
            }
            return response.json();
        }).then( data => {
            setTyps(data);
        }).catch(error => {
            console.error('Looks like there was a problem: \n', error);
        });

        fetch('https://cloud.sibvisions.com/ProjectsDemo/services/rest/ProjectsDemo/Projects/data/todos/', config).then(response => {
            if (!response.ok) {
                throw Error(response.statusText);
            }
            return response.json();
        }).then( data => {
            setTodos(data);
        }).catch(error => {
            console.error('Looks like there was a problem: \n', error);
        });
    }, [match]);

    const handleSubmit = async e => {
        e.preventDefault();        

        let items = {...project};
        Object.keys(items).forEach((key) => (items[key] == null) && delete items[key]);

        try {
            const url = `https://cloud.sibvisions.com/ProjectsDemo/services/rest/ProjectsDemo/Projects/data/projects/${match.params.id}`;
            const config = {
                method: 'PUT',
                headers: {
                    'Content-Type': 'application/json',
                    'Authorization': JSON.parse(sessionStorage.getItem('auth')).authorization
                },
                body: JSON.stringify(items)
            }
            
            const response = await fetch(url, config);

            if (!response.ok) {
                throw Error(response.statusText);
            }

            console.log(response);

        } catch (error) {
            console.error('Looks like there was a problem: \n', error);
        }
    };

    const handleChange = (e) => {
        e.persist();
        const {name, value} = e.target;

        if (name === 'TYP_ID') {
            if (value !== '') {
                setProject(project => ({...project, TYP_ID: value, TYP_TYP: typs.find(function(typ) { return typ.ID == this }, value).TYP}));
            }
            else {
                let items = {...project};
                delete items.TYP_TYP;
                delete items.TYP_ID;
                setProject(items);
            }
        } else {
            setProject(project => ({...project, [name]: value}));
        }

    }

    const handleDateChange = (name, value) => {
        if(value !== null ) {
            setProject({...project, [name]: value.toISOString().replace('Z', '+0000')});
        } else {
            let items = {...project};
            delete items[name];
            setProject(items);
        }
    }

    const handleTodoChange = (e) => {
        const {name, value, checked} = e.target;
        let doneFieldChanged = '';
        if(name === 'DONE') {
            doneFieldChanged = (checked) ? 'Y' : 'N'
        }
        let elementId = e.target.getAttribute('data-todo-id');
        let items = [...todos];
        elementId = items.map(element => element.ID).indexOf(parseInt(elementId));
        const item = { ...items[elementId], [name]: (name === 'DONE') ? doneFieldChanged : value } 
        items[elementId] = item;
        setTodos(items);
    }

    const deleteTodo = (e) => {
        const todoId = e.target.getAttribute('data-todo-id');
        let items = [...todos];
        const elementId = items.map(element => element.ID).indexOf(parseInt(todoId));
        
        const config = {
            method: 'DELETE',
            headers: {
                'Content-Type': 'application/json',
                'Authorization': JSON.parse(sessionStorage.getItem('auth')).authorization
            }
        } 
        fetch(`https://cloud.sibvisions.com/ProjectsDemo/services/rest/ProjectsDemo/Projects/data/todos/${todoId}`, config).then(response => {
            if (!response.ok) {
                throw Error(response.statusText);
            }
            console.log(response);
            
            items.splice(elementId, 1);
            setTodos(items);

        }).catch(error => {
            console.error('Looks like there was a problem: \n', error);
        });
    }

    return (
        <div>
            <h1>Project Detail</h1>
            <form onSubmit={handleSubmit}>
                <div className="form-group">
                    <label htmlFor="NAME">Name:</label>
                    <input
                        type="text"
                        id="NAME"
                        className="form-control"
                        name="NAME"
                        value={project.NAME}
                        onChange={handleChange}
                    />
                </div>
                <div className="form-group">
                    <label htmlFor="DESCRIPTION">
                        Description:
                    </label>
                    <textarea 
                        id="DESCRIPTION"
                        className="form-control"
                        name="DESCRIPTION"
                        value={project.DESCRIPTION}
                        onChange={handleChange}
                    />
                </div>
                <div className="row">
                    <div className="col form-group">
                        <label htmlFor="FROM_DATE">
                            From Date:
                        </label>
                        { project.FROM_DATE !== '' &&
                            <DatePicker id="FROM_DATE" className="form-control" selected={(project.FROM_DATE === null || project.FROM_DATE === undefined) ? null : new Date(project.FROM_DATE) } onChange={(date) => handleDateChange('FROM_DATE', date)} />
                        }
                    </div>
                    <div className="col form-group">
                        <label htmlFor="TO_DATE">
                            To Date:
                        </label>
                        { project.TO_DATE !== '' &&
                            <DatePicker id="TO_DATE" className="form-control" selected={(project.TO_DATE === null || project.TO_DATE === undefined) ? null : new Date(project.TO_DATE)} onChange={(date) => handleDateChange('TO_DATE', date)} />
                        }
                    </div>
                </div>
                <div className="form-group">
                    <label htmlFor="TYP_ID">
                        Typ:
                    </label>
                    <select id="TYP_ID" className="form-control" name="TYP_ID" value={(project.TYP_ID === null) ? '' : project.TYP_ID } onChange={handleChange}>
                        <option value=""></option>
                        { 
                            typs.map(typ => {
                                return <option key={typ.ID} value={typ.ID}>{typ.TYP}</option>
                            })
                        }
                    </select>
                </div>
                { todos.length > 0 &&
                    <Fragment>
                        <h2>Todos</h2>
                        {
                            todos.filter(todo => todo.PROJ_ID == match.params.id).map(todo => (
                                <div key={todo.ID} className="input-group mb-3">
                                    <div className="input-group-prepend">
                                        <div className="input-group-text">
                                            <input 
                                                type="checkbox"
                                                name="DONE"
                                                data-todo-id={todo.ID}
                                                checked={(todo.DONE == 'N') ? false : true}
                                                onChange={handleTodoChange} />
                                        </div>
                                    </div>
                                    <input
                                        type="text"
                                        className="form-control"
                                        name="TODO"
                                        value={todo.TODO}
                                        data-todo-id={todo.ID}
                                        onChange={handleTodoChange}
                                    />
                                    <div className="input-group-append">
                                        <button 
                                            className="btn btn-danger" 
                                            data-todo-id={todo.ID} 
                                            type="button" 
                                            onClick={deleteTodo}
                                        >
                                            X
                                        </button>
                                    </div>
                                </div>
                            ))
                        }
                    </Fragment>
                }
                <div className="d-flex justify-content-between">
                    <Link className="btn btn-secondary" to="/projects">Zurück</Link>
                    <input className="btn btn-primary" type="submit" value="Speichern" />
                </div>
            </form>
        </div>
    );
}

export default ProjectDetail;
