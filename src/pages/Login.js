import React, { useState } from 'react';

function Login(props) {
    const [errors, setErrors] = useState([]);
    const [fields, setFields] = useState({
        username: '',
        password: ''
    });

    const handleSubmit = async e => {
        try {
            const url = 'https://cloud.sibvisions.com/ProjectsDemo/services/rest/ProjectsDemo/_admin/testAuthentication';
            const config = {
                method: 'POST',
                headers: {
                    'Content-Type': 'application/json'
                },
                body: JSON.stringify({
                    username: fields.username,
                    password: fields.password
                })
            }
            
            const response = await fetch(url, config);
            
            if (!response.ok) {
                setErrors(['The username or password is incorrect']);
                throw Error(response.statusText);
            }
            
            sessionStorage.setItem('auth', JSON.stringify({authenticated: true, authorization: 'Basic '+ btoa(`${fields.username}:${fields.password}`)}))
            props.history.push("/desktop");
    
        } catch (err) {
            console.error('Looks like there was a problem: \n', err);
        }
    };

    const handleValidation = (e) => {
        e.preventDefault();

        let emptyFields = Object.entries(fields).filter(([key, value]) => value == '').map(([key]) => {
            return `The field ${key} is empty`;
        });

        setErrors(emptyFields);
        if (emptyFields.length == 0) handleSubmit();
    };
    
    const handleChange = (e) => {
        e.persist();
        setFields(fields => ({...fields, [e.target.name]: e.target.value}));
    };
    
    return (
        <div>
            <h1>Login</h1>
            { errors.length > 0 &&
                <div className="alert alert-danger" role="alert">
                    <ul>
                        {
                            errors.map((error, index) => (
                                <li key={index}>{error}</li>
                            ))
                        }
                    </ul>
                </div>
            }
            <form onSubmit={handleValidation}>
                <div className="form-group">
                    <label htmlFor="username">
                        Benutzername:
                    </label>
                    <input 
                        type="text"
                        id="username"
                        className="form-control"
                        name="username"
                        value={fields.username}
                        onChange={handleChange}
                    />
                </div>
                <div className="form-group">
                    <label htmlFor="password">
                        Passwort
                    </label>
                    <input
                        type="password"
                        id="password"
                        className="form-control"
                        name="password"
                        value={fields.password}
                        onChange={handleChange}
                    />
                </div>
                <input type="submit" className="btn btn-primary" value="Anmelden" />
            </form>
        </div>
    );
}

export default Login;