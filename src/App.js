import React from 'react';
import Nav from './components/Nav';
import Login from './pages/Login';
import Desktop from './pages/Desktop';
import Projects from './pages/Projects';
import ProjectDetail from './pages/ProjectDetail';
import { BrowserRouter as Router, Switch, Route } from 'react-router-dom';
import ProtectedRoute from './ProtectedRoute';

import './App.css';


function App() {
  return (
    <Router>
      <div className="App container">
        <Nav />
        <Switch>
          <Route path="/" exact component={Login} />
          <ProtectedRoute path="/desktop" component={Desktop} />
          <ProtectedRoute path="/projects" exact component={Projects} />
          <ProtectedRoute path="/projects/:id" component={ProjectDetail} />
        </Switch>
      </div>
    </Router>
  );
}

export default App;
