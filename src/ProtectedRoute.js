import React from 'react';
import { Route, Redirect } from 'react-router-dom';

function ProtectedRoute({ component: Component, ...rest }) {
    return (
        <Route
            {...rest} 
            render={props => {
                const auth = JSON.parse(sessionStorage.getItem('auth'));
                return ((auth == null) ? false : auth.authenticated) ? <Component {...props} /> : <Redirect to={{ pathname: "/", state: { from : props.location }}} />
            }}
        />
    );
}

export default ProtectedRoute;