import React from 'react';
import { Link, withRouter } from 'react-router-dom';

function Nav({history}) {
  return (
    <nav className="navbar navbar-light bg-light mb-3">
        <ul className="navbar-nav">
            <li className="nav-item">
                <Link className="nav-link" to="/projects">Projects</Link>
            </li>
        </ul>
        <button className="btn btn-outline-danger" onClick={() => {
            sessionStorage.removeItem('auth');
            history.push('/');
        }}>Logout</button>
    </nav>
  );
}

export default withRouter(Nav);
